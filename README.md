A repository with multiple Docker images used in the D3M program:

* `registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-xenial-python36` — Base Docker image with Python 3.6.
* `registry.gitlab.com/datadrivendiscovery/images/testing:ubuntu-xenial-python36` — Docker image with Python 3.6, Go, JavaScript/node.js used for CI testing in other repositories.
* `registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-xenial-python36-<version>` — Docker image with Python 3.6 and
  core D3M Python packages installed ([primitive-interfaces](https://gitlab.com/datadrivendiscovery/primitive-interfaces),
  [d3m-metadata](https://gitlab.com/datadrivendiscovery/metadata), and [d3m](https://gitlab.com/datadrivendiscovery/d3m)).
  `<version>` stands for a particular release of those packages. There is also an `ubuntu-xenial-python36-devel` Docker
  tag which corresponds to an image which is automatically build from the latest development code in `devel` branches
  of those packages.

See [Docker image registry](https://gitlab.com/datadrivendiscovery/images/container_registry) for current list
of all images and their tags.

`core` images (except for `devel` image) are being build and tagged with a release version of core packages and if you
use one of those tagged images you can be assured that core packages will not change under you.

Despite this though, images still change through time because base images change with security and other similar updates
to system packages and dependencies. Those updates should never introduce any observable changes, but if you really want
complete reproducibility, consider identifying the image you use with its digest-based identifier instead of a tag-based
identifier. You can use `docker images --digests` or `docker inspect` (under `RepoDigests` field) to find a digest of an
image currently available on your system.

For example, instead of using `registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-xenial-python36-devel`
identifier, which is pointing to an image which changes every time one of core packages change, use, for example,
`registry.gitlab.com/datadrivendiscovery/images/core@sha256:676121466b19c795af34dd630938206deef1992ba1a5989cfe356361fdf7c7ec`.
This identifier points to a concrete version of an image build at one point in time.
